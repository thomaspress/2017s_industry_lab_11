package ictgradschool.industry.lab11.ex01;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple GUI app that does BMI calculations.
 */
public class ExerciseOnePanel extends JPanel implements ActionListener {

    // TODO Declare JTextFields and JButtons as instance variables here.
    private JButton calculateBMI;
    private JTextField height;
    private JTextField weight;
    private JTextField bmi;
    private JButton calculateHealthy;
    private JTextField maxHealthy;

    /**
     * Creates a new ExerciseOnePanel.
     */
    public ExerciseOnePanel() {
        setBackground(Color.white);

        // TODO Construct JTextFields and JButtons.
        // HINT: Declare them as instance variables so that other methods in this class (e.g. actionPerformed) can
        // also access them.
        calculateBMI = new JButton("Calculate BMI");
        height = new JTextField(10);
        weight = new JTextField(10);
        bmi = new JTextField(10);
        calculateHealthy = new JButton("Calculate Healthy Weight");
        maxHealthy = new JTextField(10);

        // TODO Declare and construct JLabels
        // Note: These ones don't need to be accessed anywhere else so it makes sense just to declare them here as
        // local variables, rather than instance variables.
        JLabel heightLabel = new JLabel("Height in metres:");
        JLabel weightLabel = new JLabel("Weight in kilograms:");
        JLabel bmiLabelText = new JLabel("Your Body Mass index (BMI) is:");
        JLabel maxHealthyLabel = new JLabel("Maximum Healthy Weight for your Height:");


        // TODO Add JLabels, JTextFields and JButtons to window.
        // Note: The default layout manager, FlowLayout, will be fine (but feel free to experiment with others if you want!!)
        this.add(heightLabel);
        this.add(height);
        this.add(weightLabel);
        this.add(weight);
        this.add(calculateBMI);
        this.add(bmiLabelText);
        this.add(bmi);
        this.add(calculateHealthy);
        this.add(maxHealthyLabel);
        this.add(maxHealthy);



        // TODO Add Action Listeners for the JButtons
        this.calculateBMI.addActionListener(this);
        this.calculateHealthy.addActionListener(this);
    }


    /**
     * When a button is clicked, this method should detect which button was clicked, and display either the BMI or the
     * maximum healthy weight, depending on which JButton was pressed.
     */
    public void actionPerformed(ActionEvent event) {

        // TODO Implement this method.
        // Hint #1: event.getSource() will return the button which was pressed.
        // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
        // Hint #3: JTextField's setText() method will allow you to pass it a String, which will be diaplayed in the text box.

        try {
            double ht = Double.parseDouble(height.getText());
            double wt = Double.parseDouble(weight.getText());

            if (event.getSource() == calculateBMI) {
                double bmiCalculated = calculateBMI(ht, wt);
                bmi.setText("" + roundTo2DecimalPlaces(bmiCalculated));
                repaint();
            } else if (event.getSource() == calculateHealthy) {
                maxHealthy.setText("" + roundTo2DecimalPlaces(calculateMaxHealthy(ht, wt)));
            } else System.out.println("No button?");
        }
        catch (NumberFormatException e){
            System.out.println("Error " + e);
            height.setText("Required");
            weight.setText("Required");
        }
    }

    private double calculateBMI(double height, double weight){
        return (weight / (height * height));
    }

    private double calculateMaxHealthy(double height, double weight){
        return (24.9 * height * height);
    }

    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}