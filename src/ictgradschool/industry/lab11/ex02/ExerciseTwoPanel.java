package ictgradschool.industry.lab11.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener{
    //declare elements
    private JTextField num1;
    private JTextField num2;
    private JButton add;
    private JButton sub;
    private JTextField result;
    /**
     * Creates a new ExerciseFivePanel.
     */
    public ExerciseTwoPanel() {
        setBackground(Color.white);

        //create elements
        num1 = new JTextField(10);
        num2 = new JTextField(10);
        add = new JButton("Add");
        sub = new JButton("Subtract");
        JLabel resultLabel = new JLabel("Result:");
        result = new JTextField(20);
        //add listener

        this.add(num1);
        this.add(num2);
        this.add(add);
        this.add(sub);
        this.add(resultLabel);
        this.add(result);

        this.add.addActionListener(this);
        this.sub.addActionListener(this);

    }


    public void actionPerformed(ActionEvent event){

        try {
            double a = Double.parseDouble(num1.getText());
            double b = Double.parseDouble(num2.getText());

            if (event.getSource() == add) {
                result.setText("" + roundTo2DecimalPlaces(a + b));
            } else if (event.getSource() == sub) {
                result.setText("" + roundTo2DecimalPlaces(a - b));
            }
        }
        catch (NumberFormatException e){
            result.setText("You must type 2 numbers!");
        }
    }

    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}