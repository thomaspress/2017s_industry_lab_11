package ictgradschool.industry.lab11.ex04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;


/**
 * Displays an animated balloon.
 */
public class ExerciseFourPanel extends JPanel implements ActionListener, KeyListener {

    private  Balloon balloon;
//    private  JButton moveButton;
    private Timer timer;
//    private List<Balloon> balloonList = new ArrayList<>();
    private Balloon[] ballList= new Balloon[20];
    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseFourPanel() {
        setBackground(Color.white);

        for (int i = 0; i < ballList.length; i++) {
            ballList[i] = new Balloon((int)(Math.random()*1000),(int)(Math.random()*500));
        }

//        this.balloon = new Balloon(30, 60);
        timer = new Timer(50,this);
        timer.addActionListener(this);
//        this.moveButton = new JButton("Move balloon");
//        this.moveButton.addActionListener(this);
//        this.add(moveButton);
        this.addKeyListener(this);

    }

    /**
     * Moves the balloon and calls repaint() to tell Swing we need to redraw ourselves.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        for (Balloon balloon: ballList) {
            balloon.move();
        }
        // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
        // events even after we've clicked the button.
        requestFocusInWindow();

        repaint();
    }




    /**
     * Draws any balloons we have inside this panel.
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (Balloon b: ballList) {
            b.draw(g);
        }
        // Sets focus outside of actionPerformed so key presses work without pressing the button
        requestFocusInWindow();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        timer.start();
        int key = e.getKeyCode();
        switch (key){
            case 37:
                for (Balloon balloon: ballList) {
                    balloon.setDirection(Direction.Left);
                    balloon.move();
                    repaint();
                }
                break;
            case 38:
                for (Balloon balloon: ballList) {
                    balloon.setDirection(Direction.Up);
                    balloon.move();
                    repaint();
                }
                break;
            case 39:
                for (Balloon balloon: ballList) {
                    balloon.setDirection(Direction.Right);
                    balloon.move();
                    repaint();
                }
                break;
            case 40:
                for (Balloon balloon: ballList) {
                    balloon.setDirection(Direction.Down);
                    balloon.move();
                    repaint();
                }
                break;
            case 83:
                timer.stop();
                repaint();
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
}